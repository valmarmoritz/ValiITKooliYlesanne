﻿CREATE TABLE Inimene
(
	Isikukood CHAR(11) PRIMARY KEY,
	Nimi NVARCHAR(30) NOT NULL,
	Klass CHAR(5) NULL,
	Aine CHAR(5) NULL
)

CREATE TABLE KelleLapsed
(
	kood INT IDENTITY PRIMARY KEY,
	VanemaIsikukood CHAR(11),
	LapseIsikukood CHAR(11)
	-- , UNIQUE (VanemaIsikukood, LapseIsikukood)
)

CREATE TABLE Klass
(
	Klassikood CHAR(5) PRIMARY KEY,
	Nimetus NVARCHAR(30)
)

CREATE TABLE Aine
(
	Ainekood CHAR(5) PRIMARY KEY,
	Nimetus NVARCHAR(30) NOT NULL,
	Maht INT NULL  -- ainemaht tundides
)

CREATE TABLE Hinne
(
	jrknr INT IDENTITY PRIMARY KEY,
	OpilaneIK CHAR(11) NOT NULL,
	OpetajaIK CHAR(11) NULL,
	Õppeaine CHAR(5) NULL,
	Kuupäev DATETIME NULL,
	Hinne INT NOT NULL
)