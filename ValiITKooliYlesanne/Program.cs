﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ValiITKooliYlesanne
{
    class Program
    {
        static string Andmekataloog = @"..\..\andmed\";
        public static string ÕppealaJuhataja = "";

        static string oigused;
        static string rollid;
        static bool KasutajaOnÕpetaja = false;
        static bool KasutajaOnKlassijuhataja = false;
        static bool KasutajaOnÕppealajuhataja = false;
        static bool KasutajaOnÕpilane = false;
        static bool KasutajaOnLapsevanem = false;

        static void Main(string[] args)
        {
            LoeAlgAndmed();

            Console.WriteLine("Tere tulemast ValiIT! virtuaalsesse e-Kooli!");
            string kasutaja = "";
            bool sisseLogitud = false;

            while (true)
            {
                do
                {
                    Console.Write("Palun sisesta oma isikukood (väljumiseks <Enter>): ");
                    kasutaja = Console.ReadLine();
                    if (kasutaja == "") Environment.Exit(0);
                    else if (!Ihminen.dInimesed.ContainsKey(kasutaja)) Console.WriteLine("VIGA! Sellist isikukoodi meie andmebaasis ei ole!");
                }
                while (!Ihminen.dInimesed.ContainsKey(kasutaja));

                sisseLogitud = true;

                // õiguste kontroll
                OigusteKontroll(kasutaja);
                Console.WriteLine($"Tere,{rollid} {Ihminen.dInimesed[kasutaja].Nimi}.");

                // siit edasi tuleb siis nende õiguste rakendamine
                do
                {
                    Console.WriteLine($"\nSa saad: {oigused}");

                    switch (Console.ReadKey(true).KeyChar.ToString().ToUpper())
                    {
                        case "A":
                            Console.WriteLine("\nA - minu aine õpilased");
                            if (!KasutajaOnÕpetaja) Console.WriteLine("Sry, Sul pole õpetaja õigusi");

                            else
                            {
                                string aine = Ihminen.dInimesed[kasutaja].ÕpetabAinet;
                                Console.WriteLine($"aine: {Aihe.dÕppeAined[aine].AineNimi}");

                                List<Ihminen> minuÕpilased = new List<Ihminen>();
                                int loendur = 0;
                                
                                // seda peaks saama ka lihtsamalt teha
                                //foreach (var õppeklass in Luokka.dKlassid)
                                //{
                                //    foreach (var oppeaine in õppeklass.Value.ÕppeAined)
                                //    {
                                //        if (oppeaine == aine)
                                //        {
                                //            string klass = õppeklass.Value.KlassiNimi;
                                //            foreach (var opilane in Ihminen.dInimesed)
                                //            {
                                //                if (opilane.Value.ÕpibKlassis == klass)
                                //                {
                                //                    minuÕpilased.Add(opilane.Value);
                                //                    Console.WriteLine($"{++loendur:00} - {opilane.Value}");
                                //                }
                                //            }
                                //        }
                                //    }
                                //}

                                //loendur = 0;
                                //foreach (var opilane in minuÕpilased) Console.WriteLine($"{++loendur:00} - {opilane}");
                                // seda peaks saama ka lihtsamalt teha

                                minuÕpilased =
                                    Ihminen.dInimesed.Values
                                    .Where(x => x.ÕpibKlassis != "")
                                    .Where(x => Luokka.dKlassid[x.ÕpibKlassis].ÕppeAined.Contains(aine))
                                    .ToList()
                                    ;

                                loendur = 0;
                                foreach (var opilane in minuÕpilased) Console.WriteLine($"{++loendur:00} - {opilane}");

                            }
                            break;
                        case "B":
                            Console.WriteLine("\nB - minu aine õpilaste hinded");
                            if (!KasutajaOnÕpetaja) Console.WriteLine("Sry, Sul pole õpetaja õigusi");
                            else
                            { }
                            break;
                        case "C":
                            Console.WriteLine("\nC - hinde panemine");
                            if (!KasutajaOnÕpetaja) Console.WriteLine("Sry, Sul pole õpetaja õigusi");
                            else
                            { }
                            break;
                        case "D":
                            Console.WriteLine("\nD - minu klassi õpilased");
                            if (!KasutajaOnKlassijuhataja) Console.WriteLine("Sry, Sul pole klassijuhataja õigusi");
                            else
                            { }
                            break;
                        case "F":
                            Console.WriteLine("\nF - minu klassi õpilaste hinded");
                            if (!KasutajaOnKlassijuhataja) Console.WriteLine("Sry, Sul pole klassijuhataja õigusi");
                            else
                            { }
                            break;
                        case "G":
                            Console.WriteLine("\nG - meie kooli õpilased");
                            if (!KasutajaOnÕppealajuhataja) Console.WriteLine("Sry, Sul pole õppealajuhataja õigusi");
                            else
                            { }
                            break;
                        case "H":
                            Console.WriteLine("\nH - meie kooli õpilaste hinded");
                            if (!KasutajaOnÕppealajuhataja) Console.WriteLine("Sry, Sul pole õppealajuhataja õigusi");
                            else
                            { }
                            break;
                        case "I":
                            Console.WriteLine("\nI - meie kooli õpilaste vanemad");
                            if (!KasutajaOnÕppealajuhataja) Console.WriteLine("Sry, Sul pole õppealajuhataja õigusi");
                            else
                            { }
                            break;
                        case "J":
                            Console.WriteLine("\nJ - meie kooli õpilaste õed-vennad");
                            if (!KasutajaOnÕppealajuhataja) Console.WriteLine("Sry, Sul pole õppealajuhataja õigusi");
                            else
                            { }
                            break;
                        case "K":
                            Console.WriteLine("\nK - minu klassikaaslased");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "L":
                            Console.WriteLine("\nL - minu õed-vennad");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            {
                                int loendur = 0;
                                foreach (var x in Ihminen.dInimesed[kasutaja].ÕedVennad) Console.WriteLine($"{++loendur:00} - {x}");
                            }
                            break;
                        case "M":
                            Console.WriteLine("\nM - minu õdede-vendade klassikaaslased");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "N":
                            Console.WriteLine("\nN - minu õppeained");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "O":
                            Console.WriteLine("\nO - minu varasemad õppeained");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "P":
                            Console.WriteLine("\nP - minu hinded");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "R":
                            Console.WriteLine("\nR - minu klassikaaslaste hinded");
                            if (!KasutajaOnÕpilane) Console.WriteLine("Sry, Sul pole õpilase õigusi");
                            else
                            { }
                            break;
                        case "S":
                            Console.WriteLine("\nS - minu laste hinded");
                            if (!KasutajaOnLapsevanem) Console.WriteLine("Sry, Sul pole lapsevanema õigusi");
                            else
                            { }
                            break;
                        case "T":
                            Console.WriteLine("\nT - minu laste õpetajad");
                            if (!KasutajaOnLapsevanem) Console.WriteLine("Sry, Sul pole lapsevanema õigusi");
                            else
                            { }
                            break;
                        case "U":
                            Console.WriteLine("\nU - minu laste klassikaaslased");
                            if (!KasutajaOnLapsevanem) Console.WriteLine("Sry, Sul pole lapsevanema õigusi");
                            else
                            { }
                            break;
                        case "V":
                            Console.WriteLine("\nV - minu laste klassikaaslaste vanemad");
                            if (!KasutajaOnLapsevanem) Console.WriteLine("Sry, Sul pole lapsevanema õigusi");
                            else
                            { }
                            break;
                        case "X":
                            Console.WriteLine("\nX - logi välja");
                            sisseLogitud = false;
                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine("Ei saanud aru. Palun vali toiming, vajutades klahvile, mis on rea ees nurksulgudes.");
                            break;
                    }
                }
                while (sisseLogitud);
            }
        }

        public static void TrykiInimesed()      // debugimiseks kõikide hetkel teada olevate inimeste väljatrükk üle dInimesed
        {
            int jrk = 0;
            Console.WriteLine();
            Console.WriteLine("Inimesi on meil nüüd nii palju:");
            foreach (var x in Ihminen.dInimesed) Console.WriteLine($"{++jrk}: {x.Value}");
        }
        public static void LoeAlgAndmed()
        {
            // loeme sisse Õppeained: kood, nimi, tundidearv
            Console.WriteLine("Loeme sisse õppeained: ...");
            var Oppeained =
                File.ReadAllLines(Andmekataloog + "oppeained.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Aihe() { AineKood = x[0].Trim(), AineNimi = x[1].Trim(), Tunde = Convert.ToInt32(x[2].Trim()) })
                .ToList()
                ;
            foreach (var x in Oppeained)
            {
                Aihe.dÕppeAined.Add(x.AineKood, x);
                //Console.WriteLine($"{x.AineKood}, {x.AineNimi}, {x.Tunde} tundi");
            }

            // loeme sisse Õpilased: isikukood, nimi, klass
            Console.WriteLine("Loeme sisse õpilased: ...");
            var Opilased =
                File.ReadAllLines(Andmekataloog + "opilased.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Ihminen() { Isikukood = x[0].Trim(), Nimi = x[1].Trim(), ÕpibKlassis = x[2].Trim() })
                .ToList()
                ;
            foreach (var x in Opilased)
            {
                if (Ihminen.dInimesed.ContainsKey(x.Isikukood)) Ihminen.dInimesed[x.Isikukood].ÕpibKlassis = x.ÕpibKlassis;
                else Ihminen.dInimesed.Add(x.Isikukood, x);
                //Console.WriteLine(x);
                //Console.WriteLine($"{x.Nimi}, {x.Isikukood}, õpib klassis {x.ÕpibKlassis}");
            }

            // TrykiInimesed();

            // loeme sisse Õpetajad: isikukood, nimi, ainekood
            Console.WriteLine("Loeme sisse õpetajad: ...");
            var Opetajad =
                File.ReadAllLines(Andmekataloog + "opetajad.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Ihminen() { Isikukood = x[0].Trim(), Nimi = x[1].Trim(), ÕpetabAinet = x[2].Trim() })
                .ToList()
                ;
            foreach (var x in Opetajad)
            {
                if (Ihminen.dInimesed.ContainsKey(x.Isikukood)) Ihminen.dInimesed[x.Isikukood].ÕpetabAinet = x.ÕpetabAinet;
                else Ihminen.dInimesed.Add(x.Isikukood, x);
                //Console.WriteLine(x);
                //Console.WriteLine($"{x.Nimi}, {x.Isikukood}, õpetab ainet {dÕppeained[x.ÕpetabAinet].AineNimi}");
            }

            // TrykiInimesed();

            // loeme sisse Õppealajuhataja: isikukood
            Console.WriteLine("Loeme sisse õppealajuhataja: ...");
            ÕppealaJuhataja = File.ReadAllText(Andmekataloog + "oppealajuhataja.txt", Encoding.Default).ToString();
            Console.WriteLine($"Õppealajuhataja on {Ihminen.dInimesed[ÕppealaJuhataja].Nimi}");

            // loeme sisse Vanemad: isikukood, laps1(isikukood), laps2, laps3, ...
            Console.WriteLine("Loeme sisse vanemad: ...");
            var Vanemad =
                File.ReadAllLines(Andmekataloog + "vanemad.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Ihminen
                {
                    Isikukood = x[0].Trim(),
                    Lapsed = x
                        .Skip(1)
                        .Select(y => y.Trim())
                        .ToList()
                })
                .ToList()
                ;
            foreach (var vanem in Vanemad)
            {
                if (Ihminen.dInimesed.ContainsKey(vanem.Isikukood)) Ihminen.dInimesed[vanem.Isikukood].Lapsed = vanem.Lapsed;
                else Ihminen.dInimesed.Add(vanem.Isikukood, vanem);
                foreach (var laps in vanem.Lapsed) Ihminen.dInimesed[laps].Vanemad.Add(vanem.Isikukood);
            }

            TrykiInimesed();

            Console.WriteLine("\nNüüd on kõik inimesed sisse loetud!");

            // loeme sisse Klassid: lyhend, klassijuhatajaisikukood, õppeaine1, õppeaine2, ...
            Console.WriteLine("Loeme sisse klassid:...");
            var Klassid =
                File.ReadAllLines(Andmekataloog + "klassid.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Luokka()
                {
                    KlassiNimi = x[0].Trim()
                        , KlassiJuhataja = x[1].Trim()
                        , ÕppeAined = x
                            .Skip(2)
                            .Select(y => y.Trim())
                            .ToList()
                })
                .ToList()
                ;
            foreach (var x in Klassid)
            {
                Luokka.dKlassid.Add(x.KlassiNimi, x);
                Ihminen.dInimesed[x.KlassiJuhataja].KlassijuhatajaKlassis = x.KlassiNimi;
                Console.Write($"{x.KlassiNimi}, klassijuhataja {Ihminen.dInimesed[x.KlassiJuhataja].Nimi}, õppeained:");
                int loendur = 0;
                foreach (var y in x.ÕppeAined) Console.Write($"{(loendur++ > 0 ? ", " : " ")}{Aihe.dÕppeAined[y].AineNimi}");
                Console.WriteLine();
            }

            // loeme sisse Hinded: isikukood, ainekood, klass, hinne, õpetajaisikukood, kuupäev
            Console.WriteLine("Loeme sisse hinded:");
            var Hinded =
                File.ReadAllLines(Andmekataloog + "hinded.txt", Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new Arvosana()
                {
                    Õpilane = x[0].Trim()
                    , Aine = x[1].Trim()
                    , Klass = x[2].Trim()
                    , Hinne = Convert.ToInt32(x[3].Trim())
                    , Õpetaja = x[4].Trim()
                    , Kuupäev = Convert.ToDateTime(x[5].Trim())
                })
                .ToList()
                ;
            foreach (var x in Hinded)
            {
                Console.WriteLine($"Õpilane {Ihminen.dInimesed[x.Õpilane].Nimi} on saanud klassis {x.Klass} " +
                    $"kuupäeval {x.Kuupäev.ToShortDateString()} aines {Aihe.dÕppeAined[x.Aine].AineNimi} hinde {x.Hinne}.");
            }

            Console.WriteLine("Algandmed sisse loetud.\n\n");
        }
        public static void OigusteKontroll(string kasutaja)
        {
            StringBuilder sb = new StringBuilder();
            if (Ihminen.dInimesed[kasutaja].ÕpetabAinet != "")
            {
                KasutajaOnÕpetaja = true;
                sb.Append($"\n[A] näha õpilasi, kes õpivad ainet {Aihe.dÕppeAined[Ihminen.dInimesed[kasutaja].ÕpetabAinet].AineNimi}" +
                    $"\n[B] näha õpilastele aines {Aihe.dÕppeAined[Ihminen.dInimesed[kasutaja].ÕpetabAinet].AineNimi} pandud hindeid" +
                    $"\n[C] panna õpilastele aines {Aihe.dÕppeAined[Ihminen.dInimesed[kasutaja].ÕpetabAinet].AineNimi} hindeid");
            }
            if (Ihminen.dInimesed[kasutaja].KlassijuhatajaKlassis != "")
            {
                KasutajaOnKlassijuhataja = true;
                sb.Append($"\n[D] näha kõiki oma klassi {Ihminen.dInimesed[kasutaja].KlassijuhatajaKlassis} õpilasi" +
                    $"\n[F] näha oma klassi {Ihminen.dInimesed[kasutaja].KlassijuhatajaKlassis} õpilastele pandud hindeid");
            }
            if (kasutaja == ÕppealaJuhataja)
            {
                KasutajaOnÕppealajuhataja = true;
                sb.Append("\n[G] näha kõiki õpilasi" +
                    "\n[H] näha õpilaste hindeid" +
                    "\n[I] näha õpilaste vanemaid" +
                    "\n[J] näha õpilaste õdesid-vendi");
            }
            if (Ihminen.dInimesed[kasutaja].ÕpibKlassis != "")
            {
                KasutajaOnÕpilane = true;
                sb.Append("\n[K] näha oma klassikaaslasi" +
                    "\n[L] näha oma õdesid-vendi" +
                    "\n[M] nende klassikaaslasi" +
                    "\n[N] mis aineid sa õpid" +
                    "\n[O] õppinud oled" +
                    "\n[P] kõiki oma hindeid" +
                    "\n[R] oma klassikaaslaste keskmisi hindeid");
            }
            if (Ihminen.dInimesed[kasutaja].Lapsed.Count > 0)
            {
                KasutajaOnLapsevanem = true;
                sb.Append("\n[S] näha oma laste hindeid" +
                    "\n[T] näha oma laste õpetajaid" +
                    "\n[U] näha oma laste klassikaaslasi" +
                    "\n[V] näha oma laste klassikaaslaste vanemaid");
            }
            sb.Append("\n[X] välja logida");
            oigused = sb.ToString();
            rollid =
                (KasutajaOnÕpetaja ? $" õpetaja" : "")
                + (KasutajaOnKlassijuhataja ? $" ja {Ihminen.dInimesed[kasutaja].KlassijuhatajaKlassis} klassi klassijuhataja" : "")
                + (KasutajaOnÕppealajuhataja ? " ja õppealajuhataja" : "")
                + (KasutajaOnÕpilane ? " õpilane" : "")
                + (KasutajaOnLapsevanem ? " lapsevanem" : "")
                ;
        }
    }
    class Ihminen               // kõik isikud on inimesed
                                // inimesel on Täisnimi = Eesnimi(ed) + Perekonnanimi, mida muuta ei saa
                                // inimesel on unikaalne isikukood, mis algab SYYKKAA, mida muuta ei saa - see on hea Dictionary Key
                                // inimesel võib olla lapsi (kui ta on täiskasvanu?)
                                //
                                // õpilane on isik, kes õpib mingis klassis (see võib muutuda)
                                // õpilasel võib olla 1-2 vanemat (kes võivad olla ka õpetajad)
                                //
                                // õpetaja on isik, kes õpetab mingit õppeainet
                                // selles õppeaines saab õpetaja panna oma õpilastele hindeid, samas aines võib olla mitu hinnet
                                // õpetaja võib olla mingi klassi (ainuke) klassijuhataja
                                // (üks) õpetaja võib olla õppealajuhataja, see võiks olla staatiline muutuja (isikukood)
    {
        private string _EesNimi;
        private string _PereNimi;
        private string _TäisNimi;
        private string _Isikukood;
        public string EesNimi { set { _EesNimi = SuurTähtedega(value); _TäisNimi = $"{_EesNimi} {_PereNimi}"; } }
        public string PereNimi { set { _PereNimi = SuurTähtedega(value); _TäisNimi = $"{_EesNimi} {_PereNimi}"; } }

        public string Nimi { set { _TäisNimi = SuurTähtedega(value); } get { return _TäisNimi; } }
        public string Isikukood
        {
            get => _Isikukood;
            set { _Isikukood = value; }         // siia tuleks lisada isikukoodi õigsuse kontroll
        }

        public string ÕpibKlassis = "";
        public List<string> Vanemad = new List<string>();
        public List<string> Lapsed = new List<string>();
        public string ÕpetabAinet = "";
        public string KlassijuhatajaKlassis = "";

        public static Dictionary<string, Ihminen> dInimesed = new Dictionary<string, Ihminen>();

        // kas on vaja konstruktorit, mis tagaks isikukoodi unikaalsuse?
        public Ihminen()
        {
            EesNimi = "nimetu";
            PereNimi = "tundmatu";
        }

        string SuurTähtedega(string tekst)      // teeb tühikute ja/või sidekriipsudega eraldatud nimede esitähed suurteks, teised tähed väikeseks
        {
            tekst = tekst.ToLower();
            tekst = string.Join("-", tekst.Split('-').Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1)));
            tekst = string.Join(" ", tekst.Split(' ').Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1)));

            return tekst;
        }

        public IEnumerable<Ihminen> ÕedVennad
        {
            get => Ihminen.dInimesed.Values
                .Where(x => x != this)
                .Where(x => x.Vanemad
                    .Where(y => Vanemad.Contains(y))
                    .Count() > 0)
                ;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Isikukood}: {Nimi}");
            if (ÕpibKlassis != "") sb.Append($" on õpilane klassis {ÕpibKlassis}");
            if (ÕpetabAinet != "") sb.Append($" õpetab ainet {Aihe.dÕppeAined[ÕpetabAinet].AineNimi}");
            if (KlassijuhatajaKlassis != "") sb.Append($", on klassijuhataja klassis {KlassijuhatajaKlassis}");
            if (Vanemad.Count > 0)
            {
                sb.Append(", tema vanemad on:");
                foreach (var vanem in Vanemad) sb.Append($" {dInimesed[vanem].Nimi},");
            }
            if (Lapsed.Count > 0 )
            {
                sb.Append(", ta on lapsevanem ja tema lapsed on:");
                foreach (var laps in Lapsed) sb.Append($" {dInimesed[laps].Nimi},");
            }
            if (sb[sb.Length - 1] == ',') sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }
    class Luokka                        // klassil on (muutumatu) unikaalne nimi
                                        // klassis õpivad lapsed, klassil on (üks) klassijuhataja
                                        // klassis õpetatakse õppeaineid
    {
        public string KlassiNimi;
        public string KlassiJuhataja;
        public List<Ihminen> Õpilased;  // tundub ülearune, sest selle saab kätte üle dictionary dInimesed
        public List<string> ÕppeAined;
        public static Dictionary<string, Luokka> dKlassid = new Dictionary<string, Luokka>();
    }
    class Aihe                          // õppeainel on nimi, tundide arv, 1 või mitu õpetajat
                                        // unikaalne tunnus võiks olla aineKood
                                        // õpilastele antakse õppeainetes hindeid
    {
        public string AineKood;
        public string AineNimi;
        public int Tunde;
        public string[] Õpetajad;
        public static Dictionary<string, Aihe> dÕppeAined = new Dictionary<string, Aihe>();

    }
    class Arvosana                      // hindeid pannakse koos kuupäeva ja klassiga, milles see pandi (eesti keele hinne esimeses klassis 6. oktoobril)
    {
        public string Õpilane;          // isikukood
        public string Aine;
        public string Klass;
        public int Hinne;
        public string Õpetaja;
        public DateTime Kuupäev;
        public List<Arvosana> Hinded;
    }

}
